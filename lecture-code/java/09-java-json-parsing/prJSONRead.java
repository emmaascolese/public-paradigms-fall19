//simple read and parse json example
import java.io.FileReader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.*;

public class prJSONRead {

	public static void main(String[] args) throws Exception {
		Object obj = new JSONParser().parse(new FileReader("mini-movies.json"));
		
		// parsing the object obtained through parse
		JSONArray ja = (JSONArray) obj;
		
		// iterate through the array
		for (Object o : ja) {
			JSONObject jo = (JSONObject) o;
			// get movie title
			String mtitle = (String) jo.get("title");
			System.out.print(mtitle + "		");
			
			// get year
			String myearString = (String) jo.get("releaseYear");
			int myear = Integer.parseInt(myearString);
			System.out.print(myear  + "		");
			
			// get genres
			String allGenre = "";
			JSONArray genreArray  = (JSONArray) jo.get("genre");
			for (Object oGenre : genreArray) {
				String oneGenre = (String) oGenre;
				allGenre = allGenre + ", " + oneGenre;
			}	// end of genre for
			System.out.println(allGenre);
			
		}	// end of for - iterates over each movie
		
	}	// end of main
}

